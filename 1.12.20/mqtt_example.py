"""
Small example of MQTT to illustrate Python concepts:
- Classes
- List Comp
- Regex
- Lambda / anonymous function
"""

import os
import json
import time
import re

import logging
import structlog

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish


"""
Config Section
"""

PATH_ROOT = os.path.abspath(".")
PATH_LOG = os.path.join(
    PATH_ROOT,
    "1.12.20",
    "sample.log"
)

BROKER_ADDRESS = "192.168.8.151"
MQTT_TOPIC = "dat20/tut"

# log config
EXC_INFO = False
STACK_INFO = False

structlog.configure(
    processors=[
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M.%S"),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.ExceptionPrettyPrinter(),
        # structlog.processors.JSONRenderer(),
        structlog.dev.ConsoleRenderer(),

    ],
    context_class=dict,
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)

formatter = structlog.stdlib.ProcessorFormatter(
    processor=structlog.dev.ConsoleRenderer(
        colors=True
    ),
)

formatter_file = structlog.stdlib.ProcessorFormatter(
    processor=structlog.processors.JSONRenderer(),
)

"""
Class Section
"""


class Log():

    def __init__(self, name):
        self.handler = logging.StreamHandler()
        self.handler.setFormatter(formatter)

        self.file_handler = logging.FileHandler(PATH_LOG)
        self.file_handler.setFormatter(formatter_file)

        self.root_logger = logging.getLogger(name)

        self.root_logger.addHandler(self.handler)
        self.root_logger.addHandler(self.file_handler)

        self.root_logger.setLevel("INFO")
        self.logger = structlog.get_logger(name)

    def test_test(self, msg, error):
        # test basic log functionalitiy
        print("check the different log levels")
        self.logger.info(msg, error=error)
        self.logger.warning(msg, error=error)
        self.logger.error(msg, error=error)
        self.logger.critical(msg, error=error)

    """
    Loggers for general usage
    """

    def critical_general(self, msg, msg_exception):
        self.logger.critical(msg, msg_exception=msg_exception,
                             exc_info=EXC_INFO, stack_info=STACK_INFO)

    def error_general(self, msg, msg_exception):
        self.logger.error(msg, msg_exception=msg_exception,
                          exc_info=EXC_INFO, stack_info=STACK_INFO)

    def warning_general(self, msg, msg_exception):
        self.logger.warning(msg, msg_exception=msg_exception,
                            exc_info=EXC_INFO, stack_info=STACK_INFO)

    def info_general(self, msg):
        self.logger.info(msg, exc_info=EXC_INFO, stack_info=STACK_INFO)


class MQTT():

    def __init__(self, address, keep_alive, client_id, topics):
        self.address = address
        self.msg = None
        self.topic = None
        self.i_msg = 0
        self.keep_alive = keep_alive
        self.client_id = client_id
        self.topics = topics

        self.client()

        log.info_general(
            f"Successfully init MQTT client: {client_id}"
        )

    def publish_message(self, topic, msg):
        publish.single(
            topic,
            msg,
            hostname=self.address
        )

    def publish_message_log(self, msg):
        try:
            publish.single(
                MQTT_TOPIC,
                msg,
                hostname=self.address
            )
        except TypeError:
            publish.single(
                "dat20/log",
                json.dumps(msg),
                hostname=self.address
            )

    def client(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                client.connected_flag = True
                for topic in list(self.topics):
                    client.subscribe(topic)
            else:
                client.bad_connection_flag = True
                log.error_general("Can't connect to mqtt broker")

        def on_disconnect(client, userdata, rc):
            client.connected_flag = False
            client.disconnect_flag = True

        def on_message(client, userdata, msg):
            self.msg = msg.payload
            self.topic = msg.topic
            self.i_msg += 1

        def init_client_obj():
            mqtt.Client.bad_connection_flag = False
            mqtt.Client.connected_flag = False
            mqtt.Client.disconnect_flag = False

        init_client_obj()

        self.client = mqtt.Client(self.client_id)
        self.client.on_connect = on_connect
        self.client.on_message = on_message
        self.client.on_disconnect = on_disconnect
        self.client.connect(self.address, 1883, self.keep_alive)

        log.info_general(f"Successfully connect to {self.address}")
        return self

    def get_message(self):
        """
        check if there is a new message

        Returns:
            Bool: is there a new message
            Topic: mqtt topic
            Msg: mqtt msg
        """

        old_i_msg = self.i_msg
        self.client.loop(timeout=.1)

        if self.i_msg > old_i_msg:
            return True, self.topic, self.msg.decode("utf-8")

        return False, None, None

    def check_reconnect_client(self):
        """
        make a reconneciton to the mqtt broker

        Returns:
            Bool: If reconnected then return True
        """
        if self.client.disconnect_flag and not self.client.connected_flag:
            self.client.connect(self.address, 1883, self.keep_alive)
            log.info_general(
                "Reconnected to MQTT broker"
            )
            if self.client.bad_connection_flag is True:
                log.error_general(
                    "Reconnect to MQTT broker failed"
                )
                return self, False
            return self, True
        return self, False


def get_work(msg):
    pattern_work = re.compile(r"work\s->\s(.+)")

    if (match := re.match(pattern_work, msg)):
        return match.group(1)

    return "Wrong MSG"


if __name__ == "__main__":
    log = Log(__name__)

    # log.test_test("test", "some wired shit")

    mqtt_client = MQTT(
        BROKER_ADDRESS,
        keep_alive=180,
        client_id="dat20",
        topics=[MQTT_TOPIC]
    )

    mqtt_client.publish_message(MQTT_TOPIC, "hi")

    # loop paras
    idle = True
    shutdown = False
    work = False

    while True and not shutdown:
        try:
            mqtt_client, reconnect = mqtt_client.check_reconnect_client()
            new_msg, topic, msg = mqtt_client.get_message()

            if new_msg:
                if msg == "shutdown":
                    shutdown = True
                    log.warning_general("Worker shutdown")
                    mqtt_client.publish_message_log({"event": "shutdown"})
                elif msg == "idle":
                    work = False
                    idle = True
                    log.info_general("Worker goes into IDLE")
                    mqtt_client.publish_message_log({"event": "idle"})
                elif "work" in msg:
                    work = True
                    idle = False

            if work:
                mqtt_client.publish_message_log({"event": "work"})
                log.info_general("Worker goes into WORK")

                log.info_general(
                    get_work(msg)
                )

                work = False
                if shutdown:
                    log.info_general("Worker shutdown")
                    mqtt_client.publish_message_log({"event": "shutdown"})
                else:
                    idle = True
                    log.info_general("Worker goes into IDLE")
                    mqtt_client.publish_message_log({"event": "idle"})

            elif idle:
                time.sleep(5)

        except KeyboardInterrupt:
            log.info_general("User interrupt")
            break
