####################
# without          #
####################

# only works in Python 3.8 +

import re

sample_text = "hi there"
sample_text = "hello there"

re_pattern = re.compile(r"(\w+)\s(\w+)")

match = re.findall(re_pattern, sample_text)

if match[0][0] == "hi":
    print("found something")
else:
    print("not the result I want")


####################
# with             #
####################

if (match2 := re.findall(re_pattern, sample_text))[0][0].startswith("hi"):
    print(match2)
else:
    print("not the result I want")
