import os
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.image import imread


def get_stripe_size(pic_shape, magic_num, hor=True):

    if hor:
        stripe_size = round(pic_shape[0] / magic_num)
    else:
        stripe_size = round(pic_shape[1] / magic_num)

    return stripe_size


def cut_stripes(pic, magic_num, hor=True):

    stripe_size = get_stripe_size(pic.shape, magic_num, hor)

    list_stripes = []
    list_stripe = []

    if not hor:
        pic = np.rot90(pic, 1)

    for i_row, row in enumerate(pic, 0):
        list_stripe.append(row)

        if i_row % stripe_size == stripe_size - 1:
            list_stripes.append(list_stripe)
            list_stripe = []

    return list_stripes


def combine_stripes(list_stripes, hor=True):

    list_first = []
    list_second = []

    for i_stripe, stripe in enumerate(list_stripes, 0):
        if i_stripe % 2 == 0:
            list_first += stripe
        else:
            list_second += stripe

    pic = np.concatenate((list_first, list_second))

    if not hor:
        pic = np.rot90(pic, 3)

    return pic


if __name__ == "__main__":
    path_img = os.path.join(
        os.path.abspath("."),
        "1.12.20",
        "Banner-gemma.jpg"
    )

    pic = imread(path_img)

    stripes = 300

    list_stripes = cut_stripes(pic, stripes, hor=True)
    pic = combine_stripes(list_stripes, hor=True)

    plt.imshow(pic)
    plt.show()

    list_stripes = cut_stripes(pic, stripes, hor=False)
    pic = combine_stripes(list_stripes, hor=False)

    plt.imshow(pic)
    plt.show()
