import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def shrink_picture(picture, number_of_stripes, one_deeper=True):
    first_part = []
    second_part = []
    stripe_width = round(len(picture) / number_of_stripes)
    fill_first = True

    for index, row in enumerate(picture, start=0):
        if one_deeper:
            row = shrink_picture(row, number_of_stripes, False)

        if fill_first:
            first_part.append(row)
        else:
            second_part.append(row)

        if index % stripe_width == stripe_width - 1:
            fill_first = not fill_first

    return np.concatenate((first_part, second_part))


if __name__ == "__main__":
    path_img = os.path.join(
        os.path.abspath("."),
        "1.12.20",
        "Banner-gemma.jpg"
    )

    image = mpimg.imread(fname=path_img)

    plt.imshow(shrink_picture(image, 100))
    plt.show()
