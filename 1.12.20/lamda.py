import os
import pandas as pd

# https://towardsdatascience.com/apply-and-lambda-usage-in-pandas-b13a1ea037f7


def custom_rating(genre, rating):
    if 'Thriller' in genre:
        return min(10, rating+1)
    elif 'Comedy' in genre:
        return max(0, rating-1)
    else:
        return rating


# df.apply(lambda x: func(x['col1'],x['col2']),axis=1)
if __name__ == "__main__":
    path_csv = os.path.join(
        os.path.abspath("."),
        "1.12.20",
        "IMDB-Movie-Data.csv"
    )

    df = pd.read_csv(path_csv)
    print(df.columns)
    df.head()

    # df['CustomRating'] = df.apply(lambda x: custom_rating(x['Genre'], x['Rating']), axis=1)

    # get movies with tile >= 4 len
    df['num_words_title'] = df.apply(
        lambda x: len(x['Title'].split(" ")),
        axis=1)

    new_df = df[df['num_words_title'] >= 4]

    print("")

    new_df = df[df.apply(lambda x: len(x['Title'].split(" ")) >= 4, axis=1)]
