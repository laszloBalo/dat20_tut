from numpy.random import binomial


class Player():

    def __init__(self, name, skill):
        self.name = name
        self.skill = skill

        self.points = 0

    def stroke_success(self):
        """
        is the player successfully returning the ball
        this depends on the player skill

        Returns:
            Bool: True of False
        """
        # add your code here
        raise NotImplementedError()

    def create_msg(self, success):
        """
        return msg
        Do not change this function!

        Args:
            success (bool): stroke successful

        Returns:
            string: msg
        """
        if success:
            return f"{self.name} -> successful stroke"
        else:
            return f"{self.name} -> failed stroke"

    def play(self):
        success = self.stroke_success()
        msg = self.create_msg(success)
        return msg
