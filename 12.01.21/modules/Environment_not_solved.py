from time import sleep
from modules.MessageHandler import MessageHandler


class Environment():

    def __init__(self, players, path_log):
        self.players = players
        self.path_log = path_log

        self.current = None
        self.turn = 0

        self.attacker = None

    def select_player_on_turn(self):
        """
        select the player who starts the turn
        update the self.current var

        Args:
            players (DICT): storage of all players
            current (STRING): key of the current player
            turn (INT): which turn is it

        Returns:
            Player: instance of player class
        """

        # add your code here
        raise NotImplementedError()

    def select_player_action(self, current_attacker):
        """
        get the player who has to attack
        update self.attacker var

        Args:
            current_attacker (string): key of the current attacker
        """
        # add your code here
        raise NotImplementedError()

    def start_game(self):
        """
        game loop
        Do not change this function!
        """
        # looping parameters
        set_over = False
        game_over = False

        self.turn = 1

        msg_h = MessageHandler(path_log=self.path_log)

        while not set_over:
            self.select_player_on_turn()

            self.select_player_action(current_attacker=None)
            while not game_over:
                # add your code here
                raise NotImplementedError()

            self.turn += 1
            game_over = False
            # add your code here
            raise NotImplementedError()
