import re


class MessageHandler():

    def __init__(self, path_log):
        self.path_log = path_log

    def process(self, msg):
        """
        1. check msg and return game_over var
            - Use regex!
        2. save msg in csv file

        Args:
            msg (string): msg of a player

        Returns:
            bool: is the game over
        """

        re_pattern = re.compile(r"(.+)\s->\s(.*)")
        if (match := re.findall(re_pattern, msg)):
            if match[0][1] == "successful stroke":
                game_over = False
            else:
                game_over = True

            row = f"{match[0][0]};{game_over}\n"

            with open(self.path_log, "a+") as myfile:
                myfile.write(row)

        return game_over
