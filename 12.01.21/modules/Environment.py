from time import sleep
from modules.MessageHandler import MessageHandler


class Environment():

    def __init__(self, players, path_log):
        self.players = players
        self.path_log = path_log

        self.current = None
        self.turn = 0

        self.attacker = None

    def select_player_on_turn(self):
        """
        select the player who starts the turn
        update the self.current var

        Args:
            players (DICT): storage of all players
            current (STRING): key of the current player
            turn (INT): which turn is it

        Returns:
            Player: instance of player class
        """

        if self.current:
            if self.turn % 2 == 1:
                current_name = self.current.name
                self.current = self.players.get(current_name)
        else:
            player = next(iter(self.players.values()))
            self.current = player

    def select_player_action(self, current_attacker):
        """
        get the player who has to attack
        update self.attacker var

        Args:
            current_attacker (string): key of the current attacker
        """
        if current_attacker:
            current_attacker = [
                name
                for name in list(self.players.keys())
                if name != current_attacker
            ][0]
            self.attacker = self.players.get(current_attacker)
        else:
            self.attacker = self.current

    def start_game(self):
        """
        game loop
        Do not change this function!
        """
        # looping parameters
        set_over = False
        game_over = False

        self.turn = 1

        msg_h = MessageHandler(path_log=self.path_log)

        while not set_over:
            self.select_player_on_turn()

            self.select_player_action(current_attacker=None)
            while not game_over:

                msg = self.attacker.play()
                game_over = msg_h.process(msg)

                if game_over:
                    self.select_player_action(
                        current_attacker=self.attacker.name)
                    self.attacker.points += 1
                    break

                self.select_player_action(current_attacker=self.attacker.name)

            self.turn += 1
            game_over = False


            points_player_1 = self.players.get("Player_1").points
            points_player_2 = self.players.get("Player_2").points

            if points_player_1 >= 11 and points_player_2 + 1 < points_player_1:
                set_over = True
                print(f"Player_1 won")

            if points_player_2 >= 11 and points_player_1 + 1 < points_player_2:
                set_over = True
                print(f"Player_2 won")
