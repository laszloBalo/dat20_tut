import os

from modules.Player import Player
from modules.Environment import Environment


if __name__ == "__main__":

    path_csv = os.path.abspath("log/log.csv")
    if os.path.exists(path_csv):
        os.remove(path_csv)

    # this dictionary stores all players
    players = {
        "Player_1": Player(
            name="Player_1",
            skill=.7
        ),
        "Player_2": Player(
            name="Player_2",
            skill=.4
        )
    }

    env = Environment(players=players, path_log=path_csv)
    env.start_game()
