# DAT20 TUT
In this repo scripts and examples are collected in the context of a tutorial

## Exercise 3 - COVID-19

1. clone the repo and setup your python environment


could be something like that:
```{bash}
git clone https://gitlab.com/laszloBalo/dat20_tut
cd dat20_tut
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
pip install [Package]
```

2. create a jupyter notebook in the folder 15.12.20
   1. write a function which selects all valid csv files and save the paths in a list
      - valid: start with **\"Cov\"** and end with **\".csv\"**
      - the final list should have a length of 2
   2. try to make a list comprehension for the same task
3. write a **class** that processes csv files
   1. the class should take as input: path_file, sep
   2. furthermore the class should have attrib like: plot_type, file_name, df (most of them are updated on the fly)
   3. at init of the an instance the pandas df and the name should be genereated
   4. the class should have a function that generates the file_name
   5. if an instance of the class is printed the output should be the file_name
   6. write a function that takes as input a col name and returns a new col where the \",\" is replaced with \".\" (german to english num Notation)
      - use a **lambda** function
      - try it out on \"SiebenTageInzidenzFaelle\" in CovidFaelle_Timeline_GKZ.csv
   7. write a functions that takes as input a col_name and a keyword then generates a new col named \"found_[keyword]\", the processed column is searched and if the keyword is found then the result for this row is 1 else 0
       - try to use **lamda** and **regex** for this task
       - use col \"Bezirk\" in CovidFaelle_Timeline_GKZ.csv
       - use the \"(Stadt)\" substring as keyword
    8. write a function that takes converts the \"Time\" col in CovidFaelle_Timeline_GKZ.csv and converts it into datetime obj
 4. Plot
    1. Make a plot from our prepared dataset **CovidFaelle_Timeline_GKZ.csv** where you plot the top10 states (top10 is defined as the states with the max values **AnzahlFaelleSum**) of austria over Time where the **y** value is **AnzahlFaelle**
        - Plotting packages could be matplotlib, bokeh, hvplot ... your choice
        - The result should look something like this:
  ![plot](15.12.20/data/bokeh_plot.png)

-----------
## Exercise 4 - Table Tennis

In this tutorial we will recreate a simple table tennis simulation. For this we need a player class which executes moves and "sends" them to the other player. The game takes place under the usual rules of table tennis, so every 2 strokes is changed and the game is finished when the first has reached 11 points.

![panel](12.01.21/example_plot.gif)

1. clone the repo and setup your python environment
2. check out the files in folder "12.01.21" -> read the comments of the functions
3. add your lines of code where a "raise NotImplementedError()" is


## Exercise 5 - DB
In the first half we will illustrate with an example how to create and manipulate databases using SQLite, Python and SQLAlchemy. So listen and sit back. After that we will do a practical example to consolidate the knowledge.

### Setup
1. Setup your python env
2. Install packages:
   - names
   - sqlalchemy

**Linux/MacOS**
```{bash}
git clone https://gitlab.com/laszloBalo/dat20_tut
cd dat20_tut/19.01.21
python -m venv venv
source venv/bin/activate
pip install names sqlalchemy
```

Useful Tools:
- https://github.com/pawelsalawa/sqlitestudio/releases

Useful Links:
- https://docs.sqlalchemy.org/en/14/core/engines.html
- https://docs.sqlalchemy.org/en/13/orm/session_basics.html
- https://realpython.com/python-sqlite-sqlalchemy/ (detailed examples)

### Twitter Example
1. Try to rebuild the following structure -> SOMETHING IS MISSING !!!
![panel](19.01.21/structure.png)
2. Test your structure and create sample data in your database
3. Try to to filter persons that have a stats table in the last two weeks
