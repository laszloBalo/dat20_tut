import random


def partition(arr, low, high):
    pivot = arr[high]
    i = (low-1)

    for j in range(low, high):
        if arr[j] <= pivot:

            i = i+1
            arr[i], arr[j] = arr[j], arr[i]

    arr[i+1], arr[high] = arr[high], arr[i+1]
    return (i+1)


def quickSort(arr, low, high):
    if len(arr) == 1:
        return arr
    if low < high:
        pi = partition(arr, low, high)
        quickSort(arr, low, pi-1)
        quickSort(arr, pi+1, high)


def my_sort(x):
    quickSort(x, 0, len(x)-1)


list_num = [1, 2, 3, 4, 5, 6, 7, 8, 9]
random.shuffle(list_num)
print(list_num)
my_sort(list_num)

print(list_num)
