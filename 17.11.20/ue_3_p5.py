import os
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

path_file = os.path.abspath(
    os.path.join(
        os.path.dirname(__file__),
        "Banner-gemma.jpg"
    )
)


img = mpimg.imread(path_file)
# plt.imshow(img)
# plt.show()

final_img = np.empty(
    shape=(
        img.shape[0]*2,
        img.shape[1]*2,
        3
    ),
    dtype=np.int32
)

for irow, row in enumerate(img, 0):
    for icol, col in enumerate(row, 0):

        cur_color = img[irow][icol]

        if icol < len(row)-1:
            next_color_col = img[irow][icol+1]
        else:
            next_color_col = cur_color

        if irow < len(img)-1:
            next_color_row = img[irow+1][icol]
        else:
            next_color_row = cur_color

        if irow < len(img)-1 and icol < len(row)-1:
            next_color_dia = img[irow+1][icol+1]
        else:
            next_color_dia = cur_color

        new_color_col = np.array([
            np.mean([c, n], dtype=np.int32)
            for c, n in zip(cur_color, next_color_col)
        ])
        new_color_row = np.array([
            np.mean([c, n], dtype=np.int32)
            for c, n in zip(cur_color, next_color_row)
        ])

        new_color_dia = np.array([
            np.mean([c, n], dtype=np.int32)
            for c, n in zip(cur_color, next_color_dia)
        ])

        final_img[irow*2][icol*2] = cur_color
        final_img[irow*2+1][icol*2+1] = new_color_dia

        # calc mean

        final_img[irow*2+1][icol*2] = next_color_row
        final_img[irow*2][icol*2+1] = next_color_col


plt.imshow(final_img)
plt.show()
print("")
